var citas = { lunes: [
{nombre: 'Daniel', hora_inicio: '08:00', hora_termino: '09:00'}, {nombre: 'Daniel', hora_inicio: '09:30', hora_termino: '11:00'}, {nombre: 'Daniel', hora_inicio: '15:00', hora_termino: '16:00'}, {nombre: 'Daniel', hora_inicio: '17:00', hora_termino: '19:30'}
], martes: [
{nombre: 'Daniel', hora_inicio: '08:00', hora_termino: '09:00'}, {nombre: 'Daniel', hora_inicio: '11:30', hora_termino: '12:00'}, {nombre: 'Daniel', hora_inicio: '15:00', hora_termino: '16:00'}, {nombre: 'Daniel', hora_inicio: '17:00', hora_termino: '19:30'}
], miercoles: [
{nombre: 'Daniel', hora_inicio: '08:00', hora_termino: '09:00'}, {nombre: 'Daniel', hora_inicio: '10:30', hora_termino: '12:00'}, {nombre: 'Daniel', hora_inicio: '15:00', hora_termino: '16:00'}, {nombre: 'Daniel', hora_inicio: '17:00', hora_termino: '19:30'}
], jueves: [
{nombre: 'Daniel', hora_inicio: '08:00', hora_termino: '09:00'}, {nombre: 'Daniel', hora_inicio: '09:30', hora_termino: '12:00'}, {nombre: 'Daniel', hora_inicio: '15:00', hora_termino: '16:00'}, {nombre: 'Daniel', hora_inicio: '17:00', hora_termino: '19:30'}
], viernes: [
{nombre: 'Daniel', hora_inicio: '08:00', hora_termino: '09:00'}, {nombre: 'Daniel', hora_inicio: '09:30', hora_termino: '12:00'}, {nombre: 'Daniel', hora_inicio: '15:00', hora_termino: '16:00'}, {nombre: 'Daniel', hora_inicio: '17:00', hora_termino: '19:30'}
], }


var bloques = [
"08:00",
"08:30",
"09:00",
"09:30",
"10:00",
"10:30",
"11:00",
"11:30",
"12:00",
"12:30",
"13:00",
"13:30",
"14:00",
"14:30",
"15:00",
"15:30",
"16:00",
"16:30",
"17:00",
"17:30",
"18:00",
"18:30",
"19:00",
"19:30"]



function dibujarAgenda(citas){
	//dias de la semana
		innerHTMLLunes = "";
		innerHTMLMartes="";
		innerHTMLMiercoles="";
		innerHTMLJueves="";
		innerHTMLViernes="";
		// citas por dia

		if (citas.lunes.length == 0) {
			for (var j = 0; j < bloques.length; j++) {
				innerHTMLLunes = innerHTMLLunes +'<li> DISPONIBLE</li>';
			}
		};

		for ( var i = 0; i <  citas.lunes.length ; i++)
		{
			//recorrimos el bloque completo
			var tomada = false;
			for (var j = 0; j < bloques.length; j++) {
				//si encontramos hora_inicio bloqueamos
				
				if (bloques[j] == citas.lunes[i].hora_inicio)
				{
					innerHTMLLunes = innerHTMLLunes +'<li> TOMADA - '+citas.lunes[i].nombre+'</li>';
					tomada = true;
				}else if (bloques[j] == citas.lunes[i].hora_termino)
				{
					innerHTMLLunes = innerHTMLLunes +'<li> TOMADA - '+citas.lunes[i].nombre+'</li>';
					tomada = false;
					bloques = bloques.slice(bloques.indexOf(citas.lunes[i].hora_termino) + 1)
					break;
				}
				else if( tomada == false){
					innerHTMLLunes = innerHTMLLunes +'<li> DISPONIBLE</li>';
				}	
				else if( tomada == true){
					innerHTMLLunes = innerHTMLLunes +'<li> TOMADA</li>';
				}
			};
		}
		$('#ul-lunes').html(innerHTMLLunes)

bloques = [
"08:00",
"08:30",
"09:00",
"09:30",
"10:00",
"10:30",
"11:00",
"11:30",
"12:00",
"12:30",
"13:00",
"13:30",
"14:00",
"14:30",
"15:00",
"15:30",
"16:00",
"16:30",
"17:00",
"17:30",
"18:00",
"18:30",
"19:00",
"19:30"]

		if (citas.martes.length == 0) {
			for (var j = 0; j < bloques.length; j++) {
				innerHTMLMartes = innerHTMLMartes +'<li> DISPONIBLE</li>';
			}
		};

		for ( var i = 0; i <  citas.martes.length ; i++)
		{
			//recorrimos el bloque completo
			var tomada = false;
			for (var j = 0; j < bloques.length; j++) {
				//si encontramos hora_inicio bloqueamos
				
				if (bloques[j] == citas.martes[i].hora_inicio)
				{
					innerHTMLMartes = innerHTMLMartes +'<li> TOMADA - '+citas.martes[i].nombre+'</li>';
					tomada = true;
				}else if (bloques[j] == citas.martes[i].hora_termino)
				{
					innerHTMLMartes = innerHTMLMartes +'<li> TOMADA - '+citas.martes[i].nombre+'</li>';
					tomada = false;
					bloques = bloques.slice(bloques.indexOf(citas.martes[i].hora_termino) + 1)
					break;
				}
				else if( tomada == false){
					innerHTMLMartes = innerHTMLMartes +'<li> DISPONIBLE</li>';
				}	
				else if( tomada == true){
					innerHTMLMartes = innerHTMLMartes +'<li> TOMADA</li>';
				}
			};
		}
		$('#ul-martes').html(innerHTMLMartes)
bloques = [
"08:00",
"08:30",
"09:00",
"09:30",
"10:00",
"10:30",
"11:00",
"11:30",
"12:00",
"12:30",
"13:00",
"13:30",
"14:00",
"14:30",
"15:00",
"15:30",
"16:00",
"16:30",
"17:00",
"17:30",
"18:00",
"18:30",
"19:00",
"19:30"]

		if (citas.miercoles.length == 0) {
			for (var j = 0; j < bloques.length; j++) {
				innerHTMLMiercoles = innerHTMLMiercoles +'<li> DISPONIBLE</li>';
			}
		};

		for ( var i = 0; i <  citas.miercoles.length ; i++)
		{
			//recorrimos el bloque completo
			var tomada = false;
			for (var j = 0; j < bloques.length; j++) {
				//si encontramos hora_inicio bloqueamos
				
				if (bloques[j] == citas.miercoles[i].hora_inicio)
				{
					innerHTMLMiercoles = innerHTMLMiercoles +'<li> TOMADA - '+citas.miercoles[i].nombre+'</li>';
					tomada = true;
				}else if (bloques[j] == citas.miercoles[i].hora_termino)
				{
					innerHTMLMiercoles = innerHTMLMiercoles +'<li> TOMADA - '+citas.miercoles[i].nombre+'</li>';
					tomada = false;
					bloques = bloques.slice(bloques.indexOf(citas.miercoles[i].hora_termino) + 1)
					break;
				}
				else if( tomada == false){
					innerHTMLMiercoles = innerHTMLMiercoles +'<li> DISPONIBLE</li>';
				}	
				else if( tomada == true){
					innerHTMLMiercoles = innerHTMLMiercoles +'<li> TOMADA</li>';
				}
			};
		}
		$('#ul-miercoles').html(innerHTMLMiercoles)
bloques = [
"08:00",
"08:30",
"09:00",
"09:30",
"10:00",
"10:30",
"11:00",
"11:30",
"12:00",
"12:30",
"13:00",
"13:30",
"14:00",
"14:30",
"15:00",
"15:30",
"16:00",
"16:30",
"17:00",
"17:30",
"18:00",
"18:30",
"19:00",
"19:30"]

		if (citas.jueves.length == 0) {
			for (var j = 0; j < bloques.length; j++) {
				innerHTMLJueves = innerHTMLJueves +'<li> DISPONIBLE</li>';
			}
		};

		for ( var i = 0; i <  citas.jueves.length ; i++)
		{
			//recorrimos el bloque completo
			var tomada = false;
			for (var j = 0; j < bloques.length; j++) {
				//si encontramos hora_inicio bloqueamos
				
				if (bloques[j] == citas.jueves[i].hora_inicio)
				{
					innerHTMLJueves = innerHTMLJueves +'<li> TOMADA - '+citas.jueves[i].nombre+'</li>';
					tomada = true;
				}else if (bloques[j] == citas.jueves[i].hora_termino)
				{
					innerHTMLJueves = innerHTMLJueves +'<li> TOMADA - '+citas.jueves[i].nombre+'</li>';
					tomada = false;
					bloques = bloques.slice(bloques.indexOf(citas.jueves[i].hora_termino) + 1)
					break;
				}
				else if( tomada == false){
					innerHTMLJueves = innerHTMLJueves +'<li> DISPONIBLE</li>';
				}	
				else if( tomada == true){
					innerHTMLJueves = innerHTMLJueves +'<li> TOMADA</li>';
				}
			};
		}
		$('#ul-jueves').html(innerHTMLJueves)
bloques = [
"08:00",
"08:30",
"09:00",
"09:30",
"10:00",
"10:30",
"11:00",
"11:30",
"12:00",
"12:30",
"13:00",
"13:30",
"14:00",
"14:30",
"15:00",
"15:30",
"16:00",
"16:30",
"17:00",
"17:30",
"18:00",
"18:30",
"19:00",
"19:30"]
		
		if (citas.viernes.length == 0) {
			for (var j = 0; j < bloques.length; j++) {
				innerHTMLViernes = innerHTMLViernes +'<li> DISPONIBLE</li>';
			}
		};

		for ( var i = 0; i <  citas.viernes.length ; i++)
		{
			//recorrimos el bloque completo
			var tomada = false;
			for (var j = 0; j < bloques.length; j++) {
				//si encontramos hora_inicio bloqueamos
				
				if (bloques[j] == citas.viernes[i].hora_inicio)
				{
					innerHTMLViernes = innerHTMLViernes +'<li> TOMADA - '+citas.viernes[i].nombre+'</li>';
					tomada = true;
				}else if (bloques[j] == citas.viernes[i].hora_termino)
				{
					innerHTMLViernes = innerHTMLViernes +'<li> TOMADA - '+citas.viernes[i].nombre+'</li>';
					tomada = false;
					bloques = bloques.slice(bloques.indexOf(citas.viernes[i].hora_termino) + 1)
					break;
				}
				else if( tomada == false){
					innerHTMLViernes = innerHTMLViernes +'<li> DISPONIBLE</li>';
				}	
				else if( tomada == true){
					innerHTMLViernes = innerHTMLViernes +'<li> TOMADA</li>';
				}
			};
		}
		$('#ul-viernes').html(innerHTMLViernes)



}

dibujarAgenda(citas);