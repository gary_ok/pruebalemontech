var units = ["", "un ", "dos ", "tres ", "cuatro ", "cinco ", "seis ", "siete ", "ocho ", "nueve "];
var tens = ["diez ", "once ", "doce ", "trece ", "catorce ", "quince ", "dieciseis ",
"diecisiete ", "dieciocho ", "diecinueve", "veinte ", "treinta ", "cuarenta ",
"cincuenta ", "sesenta ", "setenta ", "ochenta ", "noventa "];

var hundreds= ["", "ciento ", "doscientos ", "trecientos ", "cuatrocientos ", "quinientos ", "seiscientos ",
"setecientos ", "ochocientos ", "novecientos "];

// función convierte números a letras, parametro number integer
    function numberToLetters() {
        var number = $('#number').val()
        var textNumber;
        //si el valor es cero
        if (number == 0) {
            textNumber = "cero ";
        } 
        //si es millon
        else if (number > 999999) {
            textNumber = getMillions(number + "");
        } 
        //si es miles
        else if (number > 999) {
            textNumber = getThousands(number + "");
        } 
        //si es centena
        else if (number > 99) {
            textNumber = getHundreds(number + "");
        }
        //si es decena
        else if (parseInt(number + "") > 9) {
            textNumber = getTens(number + "");
        }//sino unidades -> 9 
        else {
            textNumber = getUnits(number + "");
        }

        $('#resultado').html(textNumber);
    }

// unidades del número , parametro number string
    function getUnits(number) {
        
        var num = number.substr(number.length - 1);
        return units[parseInt(num)];
    }

// función para devolver las decenas del número, parametro number string
    function getTens(number) {
        var auxNumber = parseInt(number);
        if (auxNumber < 10) { 
            return getUnits(number);
        } else if (auxNumber > 19) {
            var auxUnits = getUnits(number);
            if (auxUnits == "") {
                return tens[parseInt(number.substr(0, 1)) + 8];
            } else {

                return tens[parseInt(number.substr(0, 1)) +8] + "y " + auxUnits;
            }
        } else {
            return tens[auxNumber - 10];
        }
    }

 //funcion para devolver las centenas del numero, parametro string number

    function getHundreds(number) {
        if (parseInt(number) > 99) {
            if (parseInt(number) == 100) {
                return " cien ";
            } else {

                return hundreds[parseInt(number.substr(0, 1))] + getTens(number.substr(1));
            }
        } else {
            return getTens(parseInt(number) + "");
        }
    }

    //función para retornar los miles del numero string number

    function getThousands(number) {

        var hundreds = number.substr(number.length - 3);

        var thousands = number.substr(0, number.length - 3);
        var auxNumber = "";

        if (parseInt(thousands) > 0) {
            auxNumber = getHundreds(thousands);
            return auxNumber + "mil " + getHundreds(hundreds);
        } else {
            return "" + getHundreds(hundreds);
        }
    }

//función para los millones del numero, parametro number string

    function  getMillions(number) {
        var miles = number.substr(number.length - 6);

        var millon = number.substr(0, number.length - 6);
        var auxNumber = "";
        if (millon.length > 1) {
            auxNumber = getHundreds(millon) + "millones ";
        } else {
            auxNumber = getUnits(millon) + "millon ";
        }
        return auxNumber + getThousands(miles);
    }