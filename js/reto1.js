

$(document).ready(function(){


   $('#calcular').click(function(){

        var number = $('#factorialNumber').val()
        //inicializamos el contador en cero
        var count = 0; 
        if (number < 0) {
            number = -1; //No existen factoriales negativos
        } else 
        //cada 5 un cero
        if (number == 5) {
            number = 1;
        } else 
            // bucle cada 5 para acumular 0
        if (number > 5) {
            for (var j = 5; number / j >= 1; j *= 5) {
                count += number / j;
            }
            number = count;
        }
    $('#resultado').html(number)
})

})


